#!/usr/bin/env python2.7

import sys
from datetime import datetime, timedelta
from sets import Set

__version__ = "1.04"
__date__ = "2017-03-27"
__author__ = "Artem Kosenko"


'''
Description
    This script checks and generates notification for nrpe_check:
    - get last part between "starting" and "finished";
    - chack the last finished date;
    - check errors in the log.

Example of /etc/nrpe.d/check_db_backup.cfg
You should manage this config in puppet!
    command[check_mysql_backup] check_db_backup.py /var/log/mysql_backup.log
    command[check_pgsql_backup] check_db_backup.py /var/log/pgsql_backup.log
'''


def EXIT_OK(msg, runtime, hard_links, new_tables, total_tables):
    seconds = 0
    seconds = int(runtime.total_seconds())
    seconds = str(seconds)
    runtime = str(runtime)
    print(msg + "\nRuntime: " + runtime + "\n" +
          "|Runtime in seconds=" + seconds +
          "; Total Tables=" + total_tables +
          "; Hard Links=" + hard_links +
          "; New Tables=" + new_tables +
          "; Total Errors=" + "0")
    sys.exit(0)


def EXIT_WARNING(msg):
    print(msg)
    sys.exit(1)


def EXIT_CRITICAL(msg, errors, runtime, errors_c, total_tables,
                  hard_links, new_tables):
    seconds = 0
    seconds = int(runtime.total_seconds())
    seconds = str(seconds)
    runtime = str(runtime)
    print(msg + "\nRuntime: " + runtime)
    for line in errors:
        print(line)
    print("|Runtime in seconds=" + seconds +
          "; Total Tables=" + total_tables +
          "; Hard Links=" + hard_links +
          "; New Tables=" + new_tables +
          "; Total Errors=" + errors_c)
    sys.exit(2)


def LOG_PATH():
    if len(sys.argv) >= 2:
        log_path = sys.argv[1]
        try:
            open(log_path)
        except IOError as e:
            msg = "WARNING - No such file or directory"
            EXIT_WARNING(msg)
        else:
            return(log_path)
    else:
        msg = "WARNING - No log specified\n" +\
              "Usage: check_db_backup.py [Path to Log]"
        EXIT_WARNING(msg)


def GET_LOG():
    # read to 'r_log' from end to beginning and get only last part of log
    r_log = []
    log_time_start = ""
    log_time_finish = ""
    finished = "No"
    id = ""
    for line in reversed(open(LOG_PATH(), 'r').readlines()):
        # get the id of part with "lockfile gefunde" and skip it
        if "lockfile gefunden" in (line).rstrip():
            id = line[20:25]
        if "finished" in line and line[20:25] != id:
            finished = "Yes"
        if "starting..." in line and finished == "Yes" and line[20:25] != id:
            log_time_start = line[0:19]
            log_time_start = log_time_start + ".000000"
            log_time_start = datetime.strptime(log_time_start,
                                               '%Y-%m-%d %H:%M:%S.%f')
            break
        r_line = line.rstrip()
        r_log.append(str(r_line))
    # reversing back in correct order
    log = []
    runtime = ""
    time_delta = ""
    current_time = datetime.now()
    id = ""
    for line in reversed(r_log):
        # get the id of part with "lockfile gefunde" and skip it
        if "lockfile gefunden" in (line).rstrip():
            id = line[20:25]
        if "finished" in (line).rstrip() and line[20:25] != id:
            log_time_finish = line[0:19]
            log_time_finish = log_time_finish + ".000000"
            log_time_finish = datetime.strptime(log_time_finish,
                                                '%Y-%m-%d %H:%M:%S.%f')
            time_delta = current_time - log_time_finish
            runtime = log_time_finish - log_time_start
            # checking if last date it too old
            if time_delta > timedelta(days=1, hours=12):
                msg = "WARNING - The beckup does not created more than " +\
                       str(time_delta) + "\n" +\
                       "Last update time is " + str(log_time_finish)
                errors = []
                errors.append("Last update time is " + str(log_time_finish))
                EXIT_WARNING(msg)
            break
        log.append(str(line))
    return(log, runtime)


def MAIN():
    errors = []
    hard_links = 0
    new_tables = 0
    total_tables = 0
    errors_c = 0
    log, runtime = GET_LOG()
    if log:
        for line in list(log):
            if "SYSCMD:mv" in line:
                new_tables += 1
            if "SYSCMD:cp -l" in line:
                hard_links += 1
            if "ERROR" in line[0:5]:
                errors_c += 1
                errors.append(str(line))
                msg = "CRITICAL - There are errors in the log"
            if not errors:
                msg = "OK - There are no errors in the log"
        total_tables = new_tables + hard_links
        hard_links = str(hard_links)
        new_tables = str(new_tables)
        total_tables = str(total_tables)
        errors_c = str(errors_c)
        if "OK" in msg:
            EXIT_OK(msg, runtime, hard_links, new_tables, total_tables)
        elif "CRITICAL" in msg:
            uniq_errors = Set(errors)
            EXIT_CRITICAL(msg, uniq_errors, runtime, errors_c, total_tables,
                          hard_links, new_tables)
    else:
        msg = "WARNING - There are no lines in the log"
        EXIT_WARNING(msg)


MAIN()
